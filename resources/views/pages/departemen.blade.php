@extends('layouts.main')
{{-- Isi Content --}}
@section('container')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data Tables</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Tables</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Data Tables</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-right mb-3">
                    <button data-toggle="modal" class="btn btn-primary" href="#" id="add_edit_function"> <i
                            class="fa fa-plus"></i> Add New Departemen</button>
                </div>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5>
                            Basic Data Tables example with
                            responsive plugin
                        </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li>
                                    <a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table width="100%" id="tabel_departemen"
                                class="text-center table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Departemen</th>
                                        <th>Nama Divisi</th>
                                        <th>Gambar</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Add Departemen -->
    <div class="modal inmodal" id="modal_add_edit_departemen" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                    <i class="fa fa-industry modal-icon"></i>
                    <h4 class="modal-title" id="modal-judul"></h4>
                </div>
                <form id="form_modal_add_edit_departemen" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 md-12">
                                <div class="form-group">
                                    <input type="hidden" name="iddepartemen" id="iddepartemen">
                                    <label>Nama departemen</label>
                                    <input autocomplete="off" name="namadepartemen" type="text" id="namadepartemen"
                                        placeholder="Enter Departemen. . . ." class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Nama divisi</label>
                                    {{-- <input autocomplete="off" name="iddivisi" type="text" id="iddivisi"
                                        placeholder="Enter Departemen. . . ." class="form-control" required> --}}
                                    <select name="iddivisi" id="iddivisi" class="form-control" data-width="100%" required>
                                        <option value="" disabled selected>-- Select Departement --</option>
                                        @foreach ($list_divisi as $d)
                                            <option value="{{ $d->intiddivisi }}">{{ $d->txtnamadivisi }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Gambar</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input id="oldfile" type="hidden" name="oldfile">
                                            <div id="tampil_gambar" class="show_gambar">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="custom-file">
                                                <input id="gambardept" name="gambardept" type="file"
                                                    class="custom-file-input gambardept" onChange="previewImg()">
                                                <label for="logo" class="custom-file-label">Choose file...</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button id="tombol_save_data_departemen" type="submit" class="btn btn-primary btn-sm"><i
                                class="fa fa-plus"></i> <span class="bold"></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //Preview Image
        function previewImg() {
            const gambar = document.querySelector('.gambardept');
            const sampulLabel = document.querySelector('.custom-file-label');
            const imgPreview = document.querySelector('.img-preview');

            // console.log(gambar.files[0]);
            //mengganti url
            sampulLabel.textContent = gambar.files[0].name;

            //mengganti preview
            const fileSampul = new FileReader();
            fileSampul.readAsDataURL(gambar.files[0]);

            fileSampul.onload = function(e) {
                imgPreview.src = e.target.result;
            }
        }

        $(document).ready(function() {


            var tabelData = $('#tabel_departemen').DataTable({
                // pageLength: 10,
                responsive: true,
                // dom: '<"html5buttons"B>lTfgitp',
                dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"col-sm-12 col-md-4"f><"col-sm-12 col-md-5"B>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                buttons: [{
                        extend: 'copy'
                    },
                    {
                        extend: 'csv'
                    },
                    {
                        extend: 'excel',
                        title: 'ExampleFile'
                    },
                    {
                        extend: 'pdf',
                        title: 'ExampleFile'
                    },

                    {
                        extend: 'print',
                        customize: function(win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "{{ route('departemen.index') }}",
                    // "type": "GET",
                },

                //Set column definition initialisation properties.
                "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                }, ],

                "columns": [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'txtnamadepartemen',
                        name: 'txtnamadepartemen'
                    },
                    {
                        data: 'txtnamadivisi',
                        name: 'txtnamadivisi'
                    },
                    {
                        data: 'gambar',
                        name: 'gambar'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    },

                ]

            });

            $(document).on('click', '#deletedepartemen', function(e) {
                e.preventDefault();
                var del_iddepartemen = $(this).attr("value");

                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger mr-2'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {

                        $.ajax({
                            url: "{{ url('departemen') }}/" + del_iddepartemen,
                            type: "delete",
                            dataType: "json",
                            data: {
                                del_iddepartemen: del_iddepartemen
                            },
                            success: function(data) {
                                if (data.respon == "success") {
                                    // $('#tabel_departemen').DataTable().destroy();
                                    // fetchdepartemen();
                                    var oTable = $('#tabel_departemen').dataTable();
                                    oTable.fnDraw(false); //reset datatable
                                    swalWithBootstrapButtons.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    );
                                } else {
                                    swalWithBootstrapButtons.fire(
                                        'Not Deleted',
                                        'Your imaginary file is safe :)',
                                        'error'
                                    );
                                }

                            }
                        });

                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                });

            });

            //TOMBOL TAMBAH DATA
            //jika tombol-tambah diklik maka
            // $('#add_edit_function').click(function () {
            $(document).on("click", "#add_edit_function", function(e) {
                e.preventDefault();
                $('#tombol_save_data_departemen').val("create-post"); //valuenya menjadi create-post
                $('#iddepartemen').val(''); //valuenya menjadi kosong
                $("#form_modal_add_edit_departemen")[0].reset();
                $(".select2").val([]).trigger("change");
                $('#modal-judul').html("Add Departemen"); //valuenya tambah departemen baru
                $('#tombol_save_data_departemen').text('Add Departemen'); //set text untuk tombol save
                validator.resetForm();
                $('.error').removeClass('error'); // clear error class

                $('#modal_add_edit_departemen').modal('show'); //modal tampil
                //Atribut Menampilkan Gambar
                $(".show_gambar").html(`
                            <img src="" width="64" class="rounded img-thumbnail img-preview">
                        `);
                $(".custom-file-label").html("Choose file");
                //End Atribut Menampilkan Gambar

            });
            //End tambah data

            /* --------------- */
            /*  Edit Records   */
            /* --------------- */
            $(document).on("click", "#editdepartemen", function(e) {
                e.preventDefault();

                var edit_iduser = $(this).attr("value");
                console.log(`
            ID Departemen : ${edit_iduser}
            `);

                validator.resetForm();
                $("#form_modal_add_edit_departemen")[0].reset();
                $('.error').removeClass('error'); // clear error class
                $(".custom-file-label").html("Choose file");


                $.ajax({
                    url: "{{ url('departemen') }}/" + edit_iduser + '/edit',
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        if (data.respon == "success") {
                            console.log(data.post);
                            $('#modal-judul').html(
                                "Edit Departemen"); //valuenya tambah pegawai baru

                            $('#tombol_save_data_departemen').text(
                                'Update Departemen'); //set text untuk tombol save
                            $('#modal_add_edit_departemen').modal('show');
                            $('#iddepartemen').val(data.post.intiddepartemen);
                            $('#namadepartemen').val(data.post.txtnamadepartemen);
                            $('select[name="iddivisi"]').val(data.post.intiddivisi).trigger(
                                'change');
                            $('input[name="oldfile"]').val(data.post.txtgambar);
                            if (data.post.txtgambar === "unknown.jpg") {
                                $(".show_gambar").html(`
                                    <img src="img/user/${data.post.txtgambar}" width="64" class="rounded img-thumbnail img-preview">
                                `);
                            } else {
                                $(".show_gambar").html(`
                                    <img src="storage/${data.post.txtgambar}" width="64" class="rounded img-thumbnail img-preview">
                                `);
                            }
                        } else {
                            toastr["error"](data.message);
                        }

                    }
                });
            });
            //End Edit Records

            //Add and update data
            if ($("#form_modal_add_edit_departemen").length > 0) {

                $.validator.addMethod('filesize', function(value, element, param) {
                    return this.optional(element) || (element.files[0].size <= param)
                }, 'File size must be less than {0}');

                var validator = $("#form_modal_add_edit_departemen").validate({
                    submitHandler: function(form) {
                        var actionType = $('#tombol_save_data_departemen').val();
                        console.log(actionType);
                        $('#tombol_save_data_departemen').html('Sending..');

                        var formData = new FormData($('#form_modal_add_edit_departemen')[0]);

                        $.ajax({
                            data: formData,
                            url: "{{ route('departemen.store') }}", //url simpan data
                            type: "POST", //karena simpan kita pakai method POST
                            cache: false,
                            contentType: false,
                            processData: false,
                            dataType: 'json', //data tipe kita kirim berupa JSON
                            success: function(data) {
                                if (data.respon == "success") {
                                    //jika berhasil
                                    console.log("suses tambah data");
                                    $('#form_modal_add_edit_departemen').trigger(
                                        "reset"); //form reset
                                    $('#modal_add_edit_departemen').modal(
                                        'hide'); //modal hide
                                    $('#tombol_save_data_departemen').html(
                                        'Simpan'); //tombol simpan
                                    var oTable = $('#tabel_departemen')
                                        .dataTable(); //inialisasi datatable
                                    oTable.fnDraw(false); //reset datatable
                                    toastr["success"](data.message);
                                } else {
                                    toastr["error"](data.message);
                                }
                            },
                            error: function(data) { //jika error tampilkan error pada console
                                console.log('Error:', data);
                                $('#tombol_save_data_departemen').html('Simpan');
                            }
                        });
                    },
                    rules: {
                        namadepartemen: {
                            required: true
                        },
                        gambardept: {
                            // required: true,
                            extension: "png|jpeg|jpg",
                            filesize: 1242880 //Byte to mb (1 MB)
                        }
                    },
                    messages: {
                        namadepartemen: {
                            required: "mohon jgn kosong ges"
                        },
                        gambardept: {
                            // required: "mohon gbr jan kosong ges",
                            extension: "tolong extensi filenya yg bener ges",
                            filesize: "tolong jgn gede ges"
                        }

                    }
                })
            }
            //End add and update data

        });
    </script>
@endpush

<link href="{{ asset('theme/HTML5_Full_version/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('theme/HTML5_Full_version/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<link href="{{ asset('theme/HTML5_Full_version/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

<link href="{{ asset('theme/HTML5_Full_version/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('theme/HTML5_Full_version/css/style.css') }}" rel="stylesheet">

<!-- Toastr -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/toastr/toastr.min.css') }}">

<!-- Mainly scripts -->
  <script src="{{asset('theme/HTML5_Full_version/js/jquery-3.1.1.min.js')}}"></script>
  <script src="{{asset('theme/HTML5_Full_version/js/popper.min.js')}}"></script>
  <script src="{{asset('theme/HTML5_Full_version/js/bootstrap.js')}}"></script>
  <script src="{{asset('theme/HTML5_Full_version/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
  <script src="{{asset('theme/HTML5_Full_version/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

  <script src="{{asset('theme/HTML5_Full_version/js/plugins/dataTables/datatables.min.js')}}"></script>
  <script src="{{asset('theme/HTML5_Full_version/js/plugins/dataTables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Custom and plugin javascript -->
  <script src="{{asset('theme/HTML5_Full_version/js/inspinia.js')}}"></script>
  <script src="{{asset('theme/HTML5_Full_version/js/plugins/pace/pace.min.js')}}"></script>

  <!-- Page-Level Scripts -->

<!-- Sweet Alert2 -->
<script type="text/javascript" src="{{ asset('js/sweetalert/sweetalert2@9.js') }}"></script>

<!-- Jquery Validate -->
<script src="{{ asset('theme/HTML5_Full_version/js/plugins/validate/jquery.validate.min.js') }}"></script>
<script src="{{ asset('theme/HTML5_Full_version/js/plugins/validate/additional-methods.min.js') }}"></script>

<!-- Toastr -->
<script type="text/javascript" src="{{ asset('js/toastr/toastr.min.js') }}"></script>

{{-- Header --}}
@include('include.header')
{{-- End Header --}}

{{-- Sidebar --}}
@include('include.sidebar')
{{-- End Sidebar --}}

{{-- Navbar --}}
@include('include.navbar')
{{-- End Navbar --}}

{{-- Isi Content    --}}
@yield('container')
{{-- End Isi Content --}}

{{-- Footer --}}
@include('include.footer')
{{-- End Footer --}}

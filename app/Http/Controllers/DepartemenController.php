<?php

namespace App\Http\Controllers;

use App\Models\Departemen;
use App\Models\Divisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class DepartemenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $list_departemen = Departemen::join('mdivisis as md', 'mdepartemens.intiddivisi', '=', 'md.intiddivisi')
        ->get();

        $list_divisi = Divisi::all();

         if ($request->ajax()) {
            return DataTables::of($list_departemen)
             ->addColumn('gambar', function ($data) {

                    if ($data->txtgambar === "unknown.jpg") {
                        $url = asset('img/user/' . $data->txtgambar);
                        $button = '<img src="' . $url . '" width="64" />';
                        return $button;
                    } else {
                        $url = asset('storage/' . $data->txtgambar);
                        $button = '<img src="' . $url . '" width="64" />';
                        return $button;
                    }
                })
                ->addColumn('action', function ($data) {
                    $button = '<a href="#" value="' . $data->intiddepartemen . '" id="editdepartemen" class="btn btn-sm btn-outline-success"><i class="fa fa-edit"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="#" value="' . $data->intiddepartemen . '" id="deletedepartemen" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash"></i></a>';
                    return $button;
                })
                ->rawColumns(['gambar','action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('pages.departemen', [
            'list_divisi' => $list_divisi
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $iddepartemen = $request->iddepartemen;

        if(empty($iddepartemen)){
          $oldFile = "unknown.jpg";
        }else{
            //filelama
          $oldFile = $request->oldfile;
        }
        // dd($request->file('gambardept'));
        if ($request->file('gambardept')) {
            $file = $request->file('gambardept')->store('upload-user');
            if ($oldFile) {
                Storage::delete($oldFile);
            }
        } else {
            $file = $oldFile;
        }

        $post   =   Departemen::updateOrCreate(
            ['intiddepartemen' => $iddepartemen],
            [
                'txtnamadepartemen' => $request->namadepartemen,
                'intiddivisi' => $request->iddivisi,
                'txtgambar' => $file,
            ]
        );



        if($iddepartemen != null){
         if ($post) {
            return response()->json(
                [
                    'post' => $post,
                    'respon' => 'success',
                    'message' => 'Sukses update data'
                ]
            );
        } else {
            return response()->json(
                [
                    'post' => $post,
                    'respon' => 'error',
                    'message' => 'Gagal update data'
                ]
            );
        }
        }else{
if ($post) {
            return response()->json(
                [
                    'post' => $post,
                    'respon' => 'success',
                    'message' => 'Sukses tambah data'
                ]
            );
        } else {
            return response()->json(
                [
                    'post' => $post,
                    'respon' => 'error',
                    'message' => 'Gagal tambah data'
                ]
            );
        }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Departemen  $departemen
     * @return \Illuminate\Http\Response
     */
    public function show(Departemen $departemen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Departemen  $departemen
     * @return \Illuminate\Http\Response
     */
    public function edit($intiddepartemen)
    {
        //
         $where = array('intiddepartemen' => $intiddepartemen);
        $post  = Departemen::where($where)->first();

        // return response()->json($post);
        if ($post) {
            return response()->json(
                [
                    'post' => $post,
                    'respon' => 'success',
                    'message' => 'Sukses edit data'
                ]
            );
        } else {
            return response()->json(
                [
                    'post' => $post,
                    'respon' => 'error',
                    'message' => 'Gagal edit data'
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Departemen  $departemen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Departemen $departemen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Departemen  $departemen
     * @return \Illuminate\Http\Response
     */
    public function destroy($intiddepartemen)
    {
        //
         $file = Departemen::find($intiddepartemen);

        if ($file->txtgambar) {
            Storage::delete($file->txtgambar);
        }

         $post = Departemen::where('intiddepartemen', $intiddepartemen)->delete();

        if ($post) {
            return response()->json(
                [
                    'data' => $post,
                    'respon' => 'success',
                    'message' => 'Sukses hapus data'
                ]
            );
        } else {
            return response()->json(
                [
                    'data' => $post,
                    'respon' => 'error',
                    'message' => 'Gagal menghapus data'
                ]
            );
        }
    }
}

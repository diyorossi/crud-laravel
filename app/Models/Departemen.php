<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departemen extends Model
{
    use HasFactory;

      protected $table = "mdepartemens";

    protected $guarded = [];
    protected $primaryKey = 'intiddepartemen';

    public function mdivisis(){
        return $this->belongsTo(Divisis::class,  'intiddivisi', 'intiddivisi');
    }
}

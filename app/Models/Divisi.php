<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    use HasFactory;

     protected $table = "mdivisis";

    protected $guarded = [];
    protected $primaryKey = 'intiddivisi';

    public function mdepartemens(){
        return $this->hasMany(Departemen::class);
    }


}

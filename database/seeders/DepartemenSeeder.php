<?php

namespace Database\Seeders;

use App\Models\Departemen;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DepartemenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
           Schema::disableForeignKeyConstraints();

        Departemen::truncate();


        $csvFile = fopen(base_path("database/data/mdepartemens.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                Departemen::create([
                    "intiddivisi" => $data['1'],
                    "txtnamadepartemen" => $data['2'],
                    "txtgambar" => $data['3']
                ]);
            }
            $firstline = false;
        }

        fclose($csvFile);
        Schema::enableForeignKeyConstraints();
    }
}

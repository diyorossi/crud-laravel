<?php

namespace Database\Seeders;

use App\Models\Divisi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DivisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
           Schema::disableForeignKeyConstraints();

        Divisi::truncate();


        $csvFile = fopen(base_path("database/data/mdivisis.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                Divisi::create([
                    "txtnamadivisi" => $data['1']
                ]);
            }
            $firstline = false;
        }

        fclose($csvFile);

        Schema::enableForeignKeyConstraints();
    }
}

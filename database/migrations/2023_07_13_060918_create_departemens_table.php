<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartemensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mdepartemens', function (Blueprint $table) {
            $table->id('intiddepartemen');
            $table->foreignId('intiddivisi')->constrained('mdivisis', 'intiddivisi')->onUpdate('cascade')->onDelete('cascade');
            $table->string('txtnamadepartemen')->nullable();
            $table->string('txtgambar')->default('unknown.jpg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mdepartemens');
    }
}
